from unittest import mock

from user_domain.user_management import get_query_instance
from user_domain.user_management.queries import Queries


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestQueryFactory:
    @mock.patch("user_domain.user_management.queries.Queries", autospec=True)
    def test_get_query_instance(self, mock_queries):
        q = get_query_instance("repository_factory", "context", "user_uuid")
        assert q.repository_factory == "repository_factory"
        assert q.context == "context"
        assert q.user_uuid == "user_uuid"


class TestUserDomainQueries:
    @mock.patch("user_domain.user_management.repositories.UserRepo")
    @mock.patch(
        "user_domain.user_management.entities.SubjectEntity", spec=True
    )
    def setup(self, SubjectEntityMock, repoMock):
        repo_factory = MockRepositoryFactory(infra_factory={})
        repo_factory.repositories["user_repository"] = repoMock
        self.queryMock = Queries(
            repository_factory=repo_factory, context=None, user_uuid=None
        )
        self.subject_entity = SubjectEntityMock

    def test_get_group_info(self):
        repo = self.queryMock.repository_factory.repositories[
            "user_repository"
        ]
        repo.get_group_info_fromDb.return_value = {"user3": "password3"}
        res = self.queryMock.get_group_info(group="group3", option="both")
        repo.get_group_info_fromDb.assert_called_once_with("group3", "both")
        assert res == {"user3": "password3"}

    def test_get_user_by_uuid(self):
        repo = self.queryMock.repository_factory.repositories[
            "user_repository"
        ]
        self.subject_entity.as_dict.return_value = {
            "id": 3,
            "uuid": "8c933fd8-f910-4948-a40b-ba20824c7865",
        }
        repo.get_subject_by_uuid.return_value = self.subject_entity

        res = self.queryMock.get_user_by_uuid(
            uuid="8c933fd8-f910-4948-a40b-ba20824c7865"
        )
        repo.get_subject_by_uuid.assert_called_once_with(
            "8c933fd8-f910-4948-a40b-ba20824c7865"
        )
        assert res == {"id": 3, "uuid": "8c933fd8-f910-4948-a40b-ba20824c7865"}
