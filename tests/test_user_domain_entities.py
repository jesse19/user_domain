from user_domain.user_management.entities import SubjectEntity


class TestSubjectEntity:
    def test_as_dic(self):
        subject_entity = SubjectEntity()
        subject_entity.id = 3
        subject_entity.uuid = "8c933fd8-f910-4948-a40b-ba20824c7865"
        subject_entity.subject_type = "employee"
        subject_entity.properties = {"telephonenumber": "1234567"}
        subject_entity.group_ids = [1, 12]

        subject_dic = subject_entity.as_dict()
        assert isinstance(subject_dic, dict)
        assert subject_dic["id"] == subject_entity.id
        assert subject_dic["uuid"] == subject_entity.uuid
        assert subject_dic["subject_type"] == subject_entity.subject_type
        assert subject_dic["properties"] == subject_entity.properties
        assert subject_dic["group_ids"] == subject_entity.group_ids

    def test_set_telephonenumber(self):
        subject_entity = SubjectEntity()
        subject_entity.uuid = "8c933fd8-f910-4948-a40b-ba20824c7865"
        subject_entity.properties = {"telephonenumber": "1234567"}

        subject_entity.set_telephonenumber(telephonenumber="0000000000")
        assert subject_entity.properties["telephonenumber"] == "0000000000"
