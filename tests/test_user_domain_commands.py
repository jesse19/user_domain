from unittest import mock

from user_domain.user_management import get_command_instance
from user_domain.user_management.commands import Commands


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestCommandFactory:
    @mock.patch("user_domain.user_management.commands.Commands", autospec=True)
    def test_get_query_instance(self, mock_commands):
        c = get_command_instance("repository_factory", "context", "user_uuid")
        assert c.repository_factory == "repository_factory"
        assert c.context == "context"
        assert c.user_uuid == "user_uuid"


class TestUserDomainCommands:
    @mock.patch("user_domain.user_management.repositories.UserRepo")
    def setup(self, repoMock):
        repo_factory = MockRepositoryFactory(infra_factory={})
        repo_factory.repositories["user_repository"] = repoMock
        self.commandMock = Commands(
            repository_factory=repo_factory,
            context=None,
            user_uuid="a60371ea-34e9-11e9-bcd8-f3f8dca3393c",
        )

    def test_add_group(self):
        repo = self.commandMock.repository_factory.repositories[
            "user_repository"
        ]
        self.commandMock.add_group(group="group8", data={"user8": "password8"})
        repo.add_group_to_Db.assert_called_once_with(
            "group8", {"user8": "password8"}
        )

    def test_change_user_telephonenumber(self):
        repo = self.commandMock.repository_factory.repositories[
            "user_repository"
        ]
        # mocking subject_enitty
        mock_subject_entity = mock.MagicMock()

        repo.get_subject_by_uuid.return_value = mock_subject_entity

        # testing change_user_telephonenumber command
        self.commandMock.change_user_telephonenumber(
            uuid="fd0be83f-3f5b-45aa-af90-91d643eea77a",
            telephonenumber="0000000000",
        )

        repo.get_subject_by_uuid.assert_called_once_with(
            "fd0be83f-3f5b-45aa-af90-91d643eea77a"
        )
        mock_subject_entity.set_telephonenumber.assert_called_once_with(
            telephonenumber="0000000000"
        )
        repo.save_subject.assert_called_once_with(
            subject_entity=mock_subject_entity
        )
