import json
from datetime import date
from unittest import mock

from zsnl_database import schema

from user_domain.user_management.entities import SubjectEntity
from user_domain.user_management.repositories import (
    SubjectMappingFactory,
    UserRepo,
)


class InfraFactoryMock:
    def __init__(self, infra):
        self.infra = infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infra[infrastructure_name]


class TestUserRepo:
    @mock.patch("sqlalchemy.orm.Session")
    def setup(self, mock_session):
        infra = InfraFactoryMock(infra={"database": mock_session})
        self.repo = UserRepo(infrastructure_factory=infra, context=None)

    def test_get_group_info_fromDb(self):
        res = self.repo.get_group_info_fromDb("group3", "both")
        assert res == {"user3": "password3"}

        res = self.repo.get_group_info_fromDb("group3", "user")
        assert res == "user3"
        res = self.repo.get_group_info_fromDb("group3", "password")
        assert res == "password3"

    def test_add_group_to_Db(self):
        self.repo.add_group_to_Db("group8", {"user8": "password8"})

    @mock.patch(
        "user_domain.user_management.repositories.SubjectMappingFactory.sqla_to_entity",
        return_value="method_called",
    )
    def test_get_subject_by_uuid(self, subject_mock):

        user = self.repo.get_subject_by_uuid(
            "8c933fd8-f910-4948-a40b-ba20824c7865"
        )

        assert user == "method_called"

    @mock.patch(
        "user_domain.user_management.repositories.SubjectMappingFactory.entity_to_sqla"
    )
    def test_save_subject(self, subject_mock):
        subject_entity = SubjectEntity()
        self.repo.save_subject(subject_entity=subject_entity)


class TestSubjectMappingFactory:
    def setup(self):
        self.subject_mapper = SubjectMappingFactory()

    def test_sqla_to_entity(self):
        subject_sqla = schema.Subject()
        subject_sqla.id = 4
        subject_sqla.uuid = "8c933fd8-f910-4948-a40b-ba20824c7865"
        subject_sqla.subject_type = "employee"
        subject_sqla.properties = "{}"
        subject_sqla.settings = "{}"
        subject_sqla.username = "beherdeer"
        subject_sqla.last_modified = date(2019, 3, 2)
        subject_sqla.group_ids = []
        subject_sqla.role_ids = []
        subject_sqla.nobody = False
        subject_sqla.system = True

        subject_entity = self.subject_mapper.sqla_to_entity(
            subject_sqla=subject_sqla
        )

        assert subject_entity.id == subject_sqla.id
        assert subject_entity.uuid == subject_sqla.uuid
        assert subject_entity.subject_type == subject_sqla.subject_type
        assert subject_entity.properties == json.loads(subject_sqla.properties)
        assert subject_entity.settings == json.loads(subject_sqla.settings)
        assert subject_entity.username == subject_sqla.username
        assert subject_entity.last_modified == subject_sqla.last_modified
        assert subject_entity.group_ids == subject_sqla.group_ids
        assert subject_entity.role_ids == subject_sqla.role_ids
        assert subject_entity.nobody == subject_sqla.nobody
        assert subject_entity.system == subject_sqla.system

    def test_entity_to_sqla(self):
        subject_entity = SubjectEntity()
        subject_entity.id = 4
        subject_entity.uuid = "8c933fd8-f910-4948-a40b-ba20824c7865"
        subject_entity.subject_type = "employee"
        subject_entity.properties = {"telephonenumber": "12345678"}
        subject_entity.settings = {"remember_casetype": "remember_casetype"}
        subject_entity.username = "beherdeer"
        subject_entity.last_modified = date(2019, 3, 5)
        subject_entity.group_ids = [1, 2, 3, 4]
        subject_entity.role_ids = [1, 2, 3, 4]
        subject_entity.nobody = False
        subject_entity.system = True

        subject_sqla = schema.Subject()
        subject_sqla.id = 4
        subject_sqla.uuid = "8c933fd8-f910-4948-a40b-ba20824c7865"
        subject_sqla.subject_type = "employee"
        subject_sqla.properties = {}
        subject_sqla.settings = {}
        subject_sqla.username = "beherdeer"
        subject_sqla.last_modified = date(2019, 3, 2)
        subject_sqla.group_ids = []
        subject_sqla.role_ids = []
        subject_sqla.nobody = False
        subject_sqla.system = True

        self.subject_mapper.entity_to_sqla(
            subject_entity=subject_entity, subject_sqla=subject_sqla
        )

        assert subject_sqla.id == subject_entity.id
        assert subject_sqla.uuid == subject_entity.uuid
        assert subject_sqla.subject_type == subject_entity.subject_type
        assert subject_sqla.properties == subject_entity.properties
        assert subject_sqla.settings == subject_entity.settings
        assert subject_sqla.username == subject_entity.username
        assert subject_sqla.last_modified == subject_entity.last_modified
        assert subject_sqla.group_ids == subject_entity.group_ids
        assert subject_sqla.role_ids == subject_entity.role_ids
        assert subject_sqla.nobody == subject_entity.nobody
        assert subject_sqla.system == subject_entity.system
