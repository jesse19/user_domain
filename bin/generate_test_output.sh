#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

mkdir -p /tmp/tests && pytest --junitxml=/tmp/tests/junit.xml --cov=user_domain --cov-report=term
