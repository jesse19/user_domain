import json

from zsnl_database import schema

from user_domain import DatabaseRepositoryBase
from user_domain.user_management.entities import SubjectEntity


class UserRepo(DatabaseRepositoryBase):
    def get_group_info_fromDb(self, group, option):
        """Retrieve group contents, given a group's name.

        :param group: group name
        :type group: string
        :param option: how to data is retrieved
        :type option: string
        :return: group object
        :rtype: dict
        """
        options = {
            "both": DATASTORE[group],
            "user": [key for key, value in DATASTORE[group].items()][0],
            "password": [value for key, value in DATASTORE[group].items()][0],
        }
        return options[option]

    def add_group_to_Db(self, group, data):
        """Add new group to Data base.

        :param group: group name
        :type group: str
        :param data: data to be saved
        :type data: object
        """
        DATASTORE[group] = data

    def get_subject_by_uuid(self, uuid):
        """Retrieve subject details from DataBase

        :param uuid: uuid of subject
        :type uuid: str
        :return: SubjectEntity
        :rtype: SubjectEntity
        """
        db = self._get_infrastructure("database")
        subject_sqla = (
            db.query(schema.Subject).filter(schema.Subject.uuid == uuid).one()
        )
        return SubjectMappingFactory().sqla_to_entity(
            subject_sqla=subject_sqla
        )

    def save_subject(self, subject_entity: SubjectEntity):
        """Save updated values for given subject entity in database.

        :param subject_entity: subject_entity entity
        :type subject_entity: SubjectEntity
        """
        db = self._get_infrastructure("database")
        subject_sqla = (
            db.query(schema.Subject)
            .filter(schema.Subject.uuid == subject_entity.uuid)
            .one()
        )
        SubjectMappingFactory().entity_to_sqla(
            subject_entity=subject_entity, subject_sqla=subject_sqla
        )


class SubjectMappingFactory:
    subject_mapping = {
        "id": "id",
        "uuid": "uuid",
        "subject_type": "subject_type",
        "properties": "properties",
        "settings": "settings",
        "username": "username",
        "last_modified": "last_modified",
        "role_ids": "role_ids",
        "group_ids": "group_ids",
        "nobody": "nobody",
        "system": "system",
    }

    def sqla_to_entity(self, subject_sqla: schema.Subject):
        """Create subject entity from sqlalchemy objects by iterating over mapping.

        :param subject_sqla: sqlalchemy subject schema
        :type subject_sqla: schema.Subject
        :return: subject entity
        :rtype: SubjectEntity
        """
        subject = SubjectEntity()
        for key, val in self.subject_mapping.items():
            param = subject_sqla.__getattribute__(val)
            if val in ["properties", "settings"]:
                subject.__setattr__(key, json.loads(param))
            else:
                subject.__setattr__(key, param)
        return subject

    def entity_to_sqla(
        self, subject_entity: SubjectEntity, subject_sqla: schema.Subject
    ):
        """Update given subject_sqla with values from given subject_entity.

        Accomplished by iterating over mapping.

        :param case_entity: subject entity
        :type case_entity: SubjectEntity
        :param case_sqla: sqlalchemy object
        :type case_sqla: schema.Subject
        """
        for key, val in self.subject_mapping.items():
            param = subject_entity.__getattribute__(key)
            subject_sqla.__setattr__(val, param)


DATASTORE = {
    "group1": {"user1": "password1"},
    "group4": {"user2": "password2"},
    "group3": {"user3": "password3"},
}
