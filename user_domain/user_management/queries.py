from pkgutil import get_data

from minty.validation import validate_with

from user_domain import QueryBase


class Queries(QueryBase):
    """Queries class for user management."""

    @property
    def repository(self):
        return self.repository_factory.get_repository(
            "user_repository", context=self.context
        )

    @validate_with(get_data(__name__, "validation/get_group_info.json"))
    def get_group_info(self, group: str, option: str):
        """Retrieve detailed information about a specific group

        :param group: Identifier of the group to retrieve details for
        :type group: string
        :param option: option to specify how the data should be retrivied
        :type option: string
        """

        return self.repository.get_group_info_fromDb(group, option)

    @validate_with(get_data(__name__, "validation/get_user_by_uuid.json"))
    def get_user_by_uuid(self, uuid: str):
        """Retrieve information about a user

        :param uuid: user uuid
        :type uuid: str
        :return: user information
        :rtype: dict
        """

        user = self.repository.get_subject_by_uuid(uuid)
        return user.as_dict()
