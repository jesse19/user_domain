from .commands import Commands
from .queries import Queries
from .repositories import UserRepo

REQUIRED_REPOSITORIES = {"user_repository": UserRepo}


def get_query_instance(repository_factory, context, user_uuid):
    return Queries(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
    )


def get_command_instance(repository_factory, context, user_uuid):
    return Commands(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
    )
