from pkgutil import get_data

from minty.cqrs import event
from minty.validation import validate_with

from user_domain import CommandBase


class Commands(CommandBase):
    """Case management domain commands."""

    @property
    def repository(self):
        return self.repository_factory.get_repository(
            "user_repository", context=self.context
        )

    @validate_with(get_data(__name__, "validation/add_group.json"))
    @event("AddGroup")
    def add_group(self, group: str, data: object):
        """Command to add new group.

        :param group: group name
        :type group: str
        :param data: data to be saved
        :type data: object
        """

        self.repository.add_group_to_Db(group, data)

    @validate_with(
        get_data(__name__, "validation/change_user_telephonenumber.json")
    )
    @event("UserTelephoneNumberChanged")
    def change_user_telephonenumber(self, uuid: str, telephonenumber: str):
        """Command to change the user telephonenumber

        :param uuid: user uuid
        :type uuid: str
        :param phone_number: new telephonenumber
        :type phone_number: str
        """
        subject_entity = self.repository.get_subject_by_uuid(uuid)
        subject_entity.set_telephonenumber(telephonenumber=telephonenumber)
        self.repository.save_subject(subject_entity=subject_entity)
