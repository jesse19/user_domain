class SubjectEntity:
    __slots__ = [
        "id",
        "uuid",
        "subject_type",
        "properties",
        "settings",
        "username",
        "last_modified",
        "role_ids",
        "group_ids",
        "nobody",
        "system",
    ]

    def __init__(self):
        """Init class with all attributes in slots initialized and set to none."""
        for slot in self.__slots__:
            self.__setattr__(slot, None)

    def _reflect(self, attr):
        """Reflect on attribute type and return JSON parse-able type.

        :param attr: attribute
        :return: converted object to correct type
        :rtype: str, int or None
        """
        if attr is None:
            return attr
        elif isinstance(attr, int):
            return int(attr)
        elif isinstance(attr, dict):
            return dict(attr)
        elif isinstance(attr, list):
            return list(attr)
        else:
            return str(attr)

    def as_dict(self):
        """Return dict with all attributes supplied in slots __slots__.

        :return: dict representation of case entity class
        :rtype: dict
        """
        result = {}
        for key in self.__slots__:
            result[key] = self._reflect(self.__getattribute__(key))
        return result

    def set_telephonenumber(self, telephonenumber: str):
        """Sets telephonemnuber of subject entity

        :param telephonenumber: telephonenumber
        :type telephonenumber: str
        """
        self.properties["telephonenumber"] = telephonenumber
