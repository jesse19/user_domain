__version__ = "0.0.6"

from minty import Base
from minty.infrastructure import InfrastructureFactory
from minty_infra_sqlalchemy import DatabaseSessionInfrastructure


class DatabaseRepositoryBase(Base):
    """Base class for Database Repository"""

    __slots__ = ["infrastructure_factory", "context"]

    REQUIRED_INFRASTRUCTURE = {
        "database": DatabaseSessionInfrastructure("zaaksysteemdb.")
    }

    def __init__(
        self, infrastructure_factory: InfrastructureFactory, context: str
    ):
        self.infrastructure_factory = infrastructure_factory
        self.context = context

    def _get_infrastructure(self, name):
        return self.infrastructure_factory.get_infrastructure(
            context=self.context, infrastructure_name=name
        )


class QueryBase(Base):
    """Base class for query classes"""

    __slots__ = ["repository_factory", "context", "user_uuid"]

    def __init__(self, repository_factory, context, user_uuid):
        self.repository_factory = repository_factory
        self.context = context
        self.user_uuid = user_uuid


class CommandBase(Base):
    """Base class for command classes"""

    __slots__ = ["repository_factory", "context", "user_uuid"]

    def __init__(self, repository_factory, context, user_uuid):
        self.repository_factory = repository_factory
        self.context = context
        self.user_uuid = user_uuid
